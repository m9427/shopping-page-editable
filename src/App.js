import React from 'react';
import './App.css';
import { Link, Switch, Route } from 'react-router-dom';
import ProductDetails from './container/ProductDetails';
import APIFailed from './container/APIFailed';
import Loader from './container/Loader/Loader';
import GotNoData from './container/GotNoData';
import Navbar from './container/Navbar';
import axios from 'axios';
import CreateProduct from './container/CreateProduct';
import DotLoader from './container/DotLoader/DotLoader';

export class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      productData: [],
      updatedItem: {},
      fetched: false,
      error: false,
      delete: -1,
      ApiMessage: '',
      ApiCode: 0,
      wait: 0,
      createdItem: {
        title: 'Enter the title here',
        description: 'Enter the description here',
        image: 'Enter Image Url',
        price: 0.0,
        category: 'Enter the category here'
      }
    }
  }

  // Fetch data once when the component is mounted
  componentDidMount = () => {

    axios('https://fakestoreapi.com/products')
      .then(response => {
        this.setState({ productData: response.data }, () => { console.log('Data set!') });
      }).then(() => {
        this.setState({ fetched: true })
      }).catch((e) => {
        this.setState({ error: true });
        console.log(e);
      });

  }

  apiStatusReset = () => {

    this.setState({ ApiMessage: '' });

  }

  // Create product cards
  createProductCards = (dataArray) => {

    return dataArray.map((item) => {
      let rating = 0, count = 0;
      (item.rating) ? rating = item.rating.rate : rating = 0;
      (item.rating) ? count = item.rating.count : count = 0;
      return (
        <div className="card col-12" key={item.id}>
          <div className="card-header">
            {item.category}
          </div>
          <div className='change-flex'>
            <img src={item.image} className="card-img-top my-img" alt="Product_Image" />
            <div className="card-body d-flex flex-column justify-content-md-between">
              <h6 className="card-title">{item.title}</h6>
              <h3> Price : &#36; {item.price}</h3>
              <p className="card-text small-text">Ratings: {rating} <br />
                Total rating count: {count}
              </p>
              <Link to={`/${item.id}`} className="btn btn-primary pointer-setter" onClick={this.apiStatusReset}>Select</Link>
            </div>
          </div>
        </div>
      );
    });

  }

  // render cards after successfull data fetch
  renderProducts = () => {

    const myCards = this.createProductCards(this.state.productData);
    return (
      <div className='container-fluid'>
        <div className="row">
          <div className="col-lg-12 d-flex flex-sm-row flex-md-column justify-content-between p-0">
            <div className='up-arrow'> <strong>&#8593;</strong></div>
            <div className='left-arrow'> <strong>&#8592;</strong></div>
            {myCards}
            <div className='container-fluid mt-2 d-flex justify-content-center'>
              <Link to={`/new`} className="btn btn-info pointer-setter" onClick={this.apiStatusReset}>Create New item</Link>
            </div>
          </div>
        </div>
      </div>
    );

  }

  // Check the API fetch and act accordingly
  checkAPILoaded = () => {

    return (
      (!this.state.fetched && !this.state.error) ?
        <Loader />
        :
        (this.state.fetched && this.state.productData) ?
          this.renderProducts()
          :
          (this.state.error) ?
            <APIFailed />
            :
            <GotNoData />
    );

  }

  // fetch the product info from here based on id
  fetchProductInfo = (id) => {

    return this.state.productData.filter((item) => item.id === id).pop();

  }

  // Change product title
  changeTitle = (id, title) => {

    const selectedObject = this.state.productData.filter((item) => item.id === parseInt(id)).pop();
    selectedObject.title = title;
    const updatedProductData = this.state.productData;
    this.setState({
      productData: updatedProductData,
      updatedItem: selectedObject
    })

  }

  // Change the description
  changeDescription = (id, desc) => {

    const selectedObject = this.state.productData.filter((item) => item.id === parseInt(id)).pop();
    selectedObject.description = desc;
    const updatedProductData = this.state.productData;
    this.setState({
      productData: updatedProductData,
      updatedItem: selectedObject
    });

  }

  // Change the price
  changePrice = (id, price) => {

    const selectedObject = this.state.productData.filter((item) => item.id === parseInt(id)).pop();
    selectedObject.price = price;
    const updatedProductData = this.state.productData;
    this.setState({
      productData: updatedProductData,
      updatedItem: selectedObject
    });

  }

  // Change the rating rate
  changeRate = (id, rate) => {
    const selectedObject = this.state.productData.filter((item) => item.id === parseInt(id)).pop();

    if (selectedObject.rating) {
      selectedObject.rating.rate = rate;
    } else {
      selectedObject['rating'] = {
        'rate': rate
      }
    }

    const updatedProductData = this.state.productData;
    this.setState({
      productData: updatedProductData
    });

  }

  // Change the rating count
  changeCount = (id, count) => {
    const selectedObject = this.state.productData.filter((item) => item.id === parseInt(id)).pop();

    if (selectedObject.rating) {
      selectedObject.rating.count = count;
    } else {
      selectedObject['rating'] = {
        'count': count
      }
    }

    const updatedProductData = this.state.productData;
    this.setState({
      productData: updatedProductData
    });
  }

  // Add a delete request value with the respective id
  deleteRequest = (id, go) => {

    if (go) {
      this.setState((state) => ({
        delete: id
      }))
    } else {
      this.setState((state) => ({
        delete: -1
      }))
    }

  }

  // Delete API Call
  deleteConfirm = (id) => {

    this.setState({ wait: 1 });

    axios.delete(`https://fakestoreapi.com/products/${id}`).then((response) => {

      if (response.request.status === 200) {
        const newArr = this.state.productData.filter((item) => item.id !== parseInt(id));
        this.setState({
          productData: newArr,
          ApiMessage: 'Delete Successful.',
          ApiCode: 1,
          wait: 0
        });
      }

    }).catch(console.log);
  }

  // Patch API call
  updateAPI = (id) => {

    this.setState({ wait: 1 });
    axios({
      method: 'patch',
      url: `https://fakestoreapi.com/products/${id}`,
      data: this.state.updatedItem,
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    })
      .then((response) => {
        if (response.request.status === 200) {
          this.setState({
            ApiMessage: 'Item Update Successful.',
            ApiCode: 1,
            wait: 0
          })
        } else {
          this.setState({
            ApiMessage: 'Update Failed!',
            ApiCode: -1,
            wait: 0
          })
        }
      })
      .catch(console.log);

  }

  // Prompt user to select an item from the side bar
  userPrompt = () => {
    return (
      <div className='container col-lg-8 col-sm-11 text-light text-center d-flex align-items-between justify-content-center h-100'>
        <div className='container col-lg-7 col-sm-10'>
          <p className='left-arrow'>
            <strong>&#8592;</strong> Please scroll the above products.
          </p>
          <p>
            Please select an item from the scroll bar by clicking on the<br /> <strong className='text-primary'>SELECT</strong> <br /> Button!
          </p>
        </div>
      </div>
    );
  }

  // Add product POST API call
  addProduct = () => {

    const dataObj= this.state.createdItem;
    this.setState({ wait: 1 });
    axios({
      method: 'post',
      url: 'https://fakestoreapi.com/products',
      data: dataObj,
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    })
      .then((response) => {

        const previousData = this.state.productData;
        previousData.push(response.data);

        if (response.request.status === 200) {
          this.setState({
            ApiMessage: 'New Item added.',
            ApiCode: 1,
            wait: 0
          })
        } else {
          this.setState({
            ApiMessage: 'Item add Failed!',
            ApiCode: -1,
            wait: 0
          })
        }

        this.setState({
          productData: previousData
        });
      })
      .catch(console.log);
  }

  // Take the data from create product and update state here
  updateCreatedItem=(dataToUpdate, field)=>{
    const selectedObject= this.state.createdItem;
    selectedObject[field]=dataToUpdate;
    this.setState((state)=>({
      createdItem: selectedObject
    }));
  }

  apiStatusUpdate = () => {
    return (
      <div className='container'>
        {
          (this.state.ApiCode === 1) ?
            <h3 className='text-center text-success bg-light'>API Status: {this.state.ApiMessage}</h3>
            :
            <h3 className='text-center text-danger bg-light'>API Status: {this.state.ApiMessage}</h3>
        }
      </div>
    )

  }




  render() {
    return (

      <>
        <Navbar />
        <div className='container-fluid'>
          <div className='row'>
            <div className='col-lg-3 col-md-4 col-sm-12 my-container d-flex flex-sm-row flex-md-column overflow-setter'>
              {this.checkAPILoaded()}
            </div>
            <div className='col-lg-8 col-md-7 col-sm-12 custom-box m-auto'>
              {(this.state.wait) ? <DotLoader /> : null}
              {(this.state.ApiMessage) ? this.apiStatusUpdate() : null}
              <Switch>
                <Route
                  path='/new' exact
                  render={
                    (props) => (
                      <CreateProduct
                        addProduct={this.addProduct}
                        updateCreatedItem={this.updateCreatedItem}
                        addStatus={this.state.addStatus}
                        createdItem={this.state.createdItem}
                        {...props}
                      />
                    )
                  }
                />
                <Route
                  path='/:id'
                  render={
                    (props) => (
                      <ProductDetails
                        changeTitle={this.changeTitle}
                        changeDescription={this.changeDescription}
                        changePrice={this.changePrice}
                        changeRate={this.changeRate}
                        changeCount={this.changeCount}
                        deleteConfirm={this.deleteConfirm}
                        deleteRequest={this.deleteRequest}
                        fetchProductInfo={this.fetchProductInfo}
                        updateAPI={this.updateAPI}
                        delete={this.state.delete}
                        {...props}
                      />
                    )
                  }
                />
                <Route
                  path='/' exact
                  component={this.userPrompt}
                />
              </Switch>
            </div>

          </div>
        </div>
      </>

    );
  }
}

export default App;
