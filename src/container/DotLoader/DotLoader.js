import React from 'react';
import './DotLoader.css';

function DotLoader() {

    return (
        <div className="lds-ellipsis"><div></div><div></div><div></div></div>
    );
}

export default DotLoader;