import React from 'react'

function APIFailed() {
    return (
        <div className='container-fluid'>
            <h2 className='text-danger text-center'> <strong>Awww.. Snap!!!</strong><br /> Could not fetch the requested data. <br /><small> We have noted this issue and are working on the fix.</small></h2>
        </div>
    )
}

export default APIFailed