import React from 'react';
import './Loader.css';

function Loader() {
    return (
        <div className="container-fluid d-flex flex-column align-items-center mt-4">
            <div className="lds-roller"><div></div><div></div><div></div></div>
            <h2 className='text-secondary text-center'> Please wait... <br />Good things take time.</h2>
        </div>
    );
}

export default Loader;