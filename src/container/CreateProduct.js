
import React, { Component } from 'react';

export class CreateProduct extends Component {


    // Add the title 
    changeTitle = (event) => {

        if (event.target.value.trim() === '') {

            this.props.updateCreatedItem(event.target.value.trim(), 'title');

        } else {

            this.props.updateCreatedItem(event.target.value, 'title');

        }

    }

    // Add the description
    changeDescription = (event) => {

        if (event.target.value.trim() === '') {


            this.props.updateCreatedItem(event.target.value.trim(), 'description');

        } else {

            this.props.updateCreatedItem(event.target.value, 'description');

        }

    }

    // Add the price
    changePrice = (event) => {

        let enteredPriceValue = event.target.value * 1;

        if (!isNaN(enteredPriceValue) && enteredPriceValue >= 0) {

            this.props.updateCreatedItem(event.target.value, 'price');

        }

    }

    // Add the category
    changeCategory = (event) => {

        if (event.target.value.trim() === '') {

            this.props.updateCreatedItem(event.target.value.trim(), 'category');

        } else {

            this.props.updateCreatedItem(event.target.value, 'category');

        }
    }

    // Add the image
    changeImage = (event) => {

        this.props.updateCreatedItem(event.target.value, 'image');

    }

    renderProductDetails = () => {

        const gotData = this.props.createdItem;

        return (
            <div className='container col-lg-8 col-md-10 col-sm-12 p-0 mt-md-4'>
                <div className="card mb-3">
                    <div className="row g-0">

                        <div className="col-md-4 d-flex flex-column justify-content-center align-items-center">
                            <img src={gotData.image} className="img-fluid rounded-start image-sizing" alt="Product_Image" />
                            <input type="text" className="form-control form-control-sm col-2 small-text" id="editImage" value={gotData.image} onChange={this.changeImage} required />
                        </div>

                        <div className="col-md-8">
                            <div className="card-body">

                                <h5 className="card-title"><input type="text" className="form-control col-12 small-text" id="editTitle" placeholder="Change title here" value={gotData.title} onChange={this.changeTitle} />
                                    {
                                        (gotData.title === '') ?
                                            <div className='small-text text-danger'>Title cannot be blank</div>
                                            :
                                            null
                                    }
                                </h5>

                                <div className="card-text small-text"><textarea className="form-control small-text" id="editDesc" placeholder="Change Description here" rows="3" value={gotData.description} onChange={this.changeDescription} />
                                    {
                                        (gotData.description === '') ?
                                            <div className='small-text text-danger'>Description cannot be blank</div>
                                            :
                                            null
                                    }
                                </div>

                                <h2> Price: &#36; <input type="text" className="form-control col-12 small-text" id="editPrice" placeholder="Change price here" value={gotData.price} onChange={this.changePrice} />
                                    {
                                        (gotData.price === '') ?
                                            <div className='small-text text-danger'>Please enter a value for price</div>
                                            :
                                            null
                                    }
                                </h2>

                                <div className='d-flex align-items-center justify-content-between'>
                                    <div className="btn btn-primary m-2" onClick={() => { this.props.addProduct() }}>Add new item</div>
                                </div>

                                <div className="card-text"><small className="text-muted">Category: <input type="text" className="form-control form-control-sm col-2 small-text" id="editCategory" value={gotData.category} onChange={this.changeCategory} required /></small></div>
                                {
                                    (gotData.category === '') ?
                                        <div className='small-text text-danger'>Category cannot be blank</div>
                                        :
                                        null
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }





    render() {
        return (

            <>
                {this.renderProductDetails()}

            </>

        );
    }
}

export default CreateProduct