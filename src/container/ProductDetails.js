
import React, { Component } from 'react';
import { Redirect } from "react-router-dom";

export class ProductDetails extends Component {


    changeRate = (event) => {
        const id = this.props.match.params.id;
        // console.log("Change Rate called", event.target.value)
        let enteredRateValue = event.target.value.trim();
        if (!isNaN(enteredRateValue * 1) && enteredRateValue >= 0 && enteredRateValue <= 5) {
            this.props.changeRate(id, enteredRateValue);
        }
    }

    changeCount = (event) => {
        const id = this.props.match.params.id;
        // console.log("Change Count called", event.target.value)
        let enteredCountValue = event.target.value * 1;
        if (!isNaN(enteredCountValue) && enteredCountValue >= 0) {
            this.props.changeCount(id, enteredCountValue);
        }

    }

    deleteRequest =() =>{
        const id = this.props.match.params.id;
        this.props.deleteRequest(id,true);
    }

    renderProductDetails = () => {
        // console.log('ID:', this.props.match.params.id);
        const productId = parseInt(this.props.match.params.id);
        const gotData = this.props.fetchProductInfo(productId);
        
        let rating = 0, count = 0;
        (gotData.rating) ? rating = gotData.rating.rate : rating = 0;
        (gotData.rating) ? count = gotData.rating.count : count = 0;
        return (
            <div className='container col-lg-8 col-md-10 col-sm-12 p-0 mt-md-4'>
                <div className="card mb-3">
                    <div className="row g-0">
                        <div className="col-md-4 d-flex flex-column justify-content-center align-items-center">
                            <img src={gotData.image} className="img-fluid rounded-start image-sizing" alt="Product_Image" />
                        </div>
                        <div className="col-md-8">
                            <div className="card-body">
                                <h5 className="card-title">{gotData.title}</h5>
                                <p className="card-text small-text">{gotData.description}</p>
                                <h2> Price: &#36; {gotData.price}</h2>
                                <div className='d-flex align-items-center justify-content-between'><div className="btn btn-primary m-2" onClick={this.deleteRequest}>Delete item</div>
                                    <div className='col-3'>
                                        <input type="text" className="form-control form-control-sm col-2 small-text" id="editRatingRate" value={rating} onChange={this.changeRate} required />
                                        {
                                            (rating === '') ?
                                                <div className='small-text text-danger'>Please enter a value</div>
                                                :
                                                null
                                        }
                                    </div>
                                    <div className='col-3'>
                                        <input type="text" className="form-control form-control-sm col-2 small-text" id="editRatingCount" value={count} onChange={this.changeCount} required />
                                        {
                                            (rating > 0 && !count) ?
                                                <div className='small-text text-danger'> Someone has rated. Cannot be zero!</div>
                                                :
                                                null
                                        }
                                    </div>
                                </div>
                                <p className="card-text"><small className="text-muted">Category: {gotData.category}</small></p>
                                {
                                    (this.props.delete=== this.props.match.params.id)?
                                    <div>
                                    <div className="btn btn-success m-2" onClick={()=>this.props.deleteConfirm(this.props.match.params.id)}>Confirm delete</div>
                                    <div className="btn btn-danger m-2" onClick={()=>this.props.deleteRequest(this.props.match.params.id,false)}>Not Sure</div>
                                    </div>
                                    :
                                    <div className="btn btn-primary" onClick={()=>this.props.updateAPI(this.props.match.params.id)}>Update Item</div>
                                    
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    changeTitle = (event) => {
        const id = this.props.match.params.id;
        // console.log("Change title called", event.target.value)
        if (event.target.value.trim() === '') {
            this.props.changeTitle(id, event.target.value.trim());
        } else {
            this.props.changeTitle(id, event.target.value);
        }

    }

    changeDescription = (event) => {
        const id = this.props.match.params.id;
        // console.log("Change Desc called", event.target.value)
        if (event.target.value.trim() === '') {
            this.props.changeDescription(id, event.target.value.trim());
        } else {
            this.props.changeDescription(id, event.target.value);
        }

    }

    changePrice = (event) => {
        const id = this.props.match.params.id;
        // console.log("Change Price called", event.target.value)
        let enteredPriceValue = event.target.value * 1;
        if (!isNaN(enteredPriceValue) && enteredPriceValue >= 0) {
            this.props.changePrice(id, event.target.value.trim());
        }
    }

    renderValueEditor = () => {
        const productId = parseInt(this.props.match.params.id);
        const gotData = this.props.fetchProductInfo(productId);
        return (
            <div className='container col-lg-8 col-md-10 col-sm-12'>
                <div className="mb-2">
                    <label htmlFor="editTitle" className="form-label small-text text-light">Please type here the new product title</label>
                    <input type="text" className="form-control col-12 small-text" id="editTitle" placeholder="Change title here" value={gotData.title} onChange={this.changeTitle} />
                    {
                        (gotData.title === '') ?
                            <div className='small-text text-danger'>Title cannot be blank</div>
                            :
                            null
                    }
                </div>
                <div className="mb-3">
                    <label htmlFor="editDesc" className="form-label small-text text-light">Please type new description here</label>
                    <textarea className="form-control small-text" id="editDesc" placeholder="Change Description here" rows="3" value={gotData.description} onChange={this.changeDescription}></textarea>
                    {
                        (gotData.description === '') ?
                            <div className='small-text text-danger'>Description cannot be blank</div>
                            :
                            null
                    }
                </div>
                <div className="mb-2">
                    <label htmlFor="editPrice" className="form-label small-text text-light">Please type here the new product price</label>
                    <input type="text" className="form-control col-12 small-text" id="editPrice" placeholder="Change price here" value={gotData.price} onChange={this.changePrice} />
                    {
                        (gotData.price === '') ?
                            <div className='small-text text-danger'>Please enter a value for price</div>
                            :
                            null
                    }
                </div>
            </div>
        );
    }

    render() {
        const gotData = this.props.fetchProductInfo(parseInt(this.props.match.params.id));
        return (
            (!gotData) ?
                <Redirect to="/" /> :

                <>
                    {this.renderProductDetails()}
                    {this.renderValueEditor()}
                </>

        );
    }
}

export default ProductDetails