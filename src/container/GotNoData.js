import React from 'react';

function GotNoData() {
    return (
        <div className='container-fluid'>
            <h1 className='text-center text-danger'> OOPS...<br /> There is no data for the page requested.</h1>
        </div>
    );
}

export default GotNoData;